/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatrice;

import static java.lang.System.in;
import java.util.Scanner;

/**
 *
 * @author marcr
 */
public class Calculatrice {
    
    public static double somme(double nombre1, double nombre2){
        return nombre1 + nombre2;
    }
    public static double soustraction(double nombre1, double nombre2){
        return nombre1 - nombre2;
    }
    public static double multiplication(double nombre1, double nombre2){
        return nombre1 * nombre2;
    }
    public static double division(double dividande, double diviseur){
        double resultat = 0;
        if (diviseur != 0){
            resultat = dividande / diviseur;
        }
        else {
            System.out.println("Division par 0 impossible");
        }
        return resultat;
    }
    
    public static double moyenne(double tableauDeNombre[]){
        double moyenne = 0;
        double sommeNombre = 0;
        
        for (double nombre : tableauDeNombre){
            sommeNombre += nombre;
        }
        
        moyenne = division(sommeNombre, tableauDeNombre.length);
        //commentaire
        return moyenne;    
    }    
}
