/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculatrice;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author marcr
 */
public class CalculatriceTest {
    
    /**
     * Test of somme method, of class Calculatrice.
     */
    @Test
    public void testSomme() {
        assertEquals(6.2, Calculatrice.somme(2.5, 3.7), 0.0);
    }

    /**
     * Test of soustraction method, of class Calculatrice.
     */
    @Test
    public void testSoustraction() {
        double nombre1 = 5.0;
        double nombre2 = 2.5;
        double resultatAttendu = 2.5;
        
        double resultatObtenu = Calculatrice.soustraction(nombre1, nombre2);
        
        assertEquals(resultatAttendu, resultatObtenu, 0.0001);
    }

    @Test
    public void testMultiplication() {
        double nombre1 = 2.5;
        double nombre2 = 3.0;
        
        double resultatAttendu = 7.5;
        double resultatObtenu = Calculatrice.multiplication(nombre1, nombre2);
        
        assertEquals(resultatAttendu, resultatObtenu, 0.0001);
    }

    @Test
    public void testDivision() {
        double dividande = 10.0;
        double diviseur = 5.0;
        
        double resultatAttendu = 2.0;
        double resultatObtenu = Calculatrice.division(dividande, diviseur);
        
        assertEquals(resultatAttendu, resultatObtenu, 0.0001);
    }

    @Test
    public void testMoyenne() {
        double[] tableauDeNombre = {1.0, 2.0, 3.0, 4.0, 5.0};
        
        double resultatAttendu = 3.0;
        double resultatObtenu = Calculatrice.moyenne(tableauDeNombre);
        
        assertEquals(resultatAttendu, resultatObtenu, 0.0001);
    }
    
}
